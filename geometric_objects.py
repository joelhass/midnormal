##################################################################################

class Point:
  def __init__(self, x, y, z):
    self.x = float(x)
    self.y = float(y)
    self.z = float(z)
    self.cutoff = 0.00000001

  def __str__(self):
    return "Point: (%s, %s, %s)" % (self.x, self.y, self.z)
       
  @staticmethod
  def create_from_grid_coordinates(i, j, k, dx, dy, dz):
    x = (float(i) - (0.5) * (j % 2)) * dx
    y = (float(j)) * dy
    z = float(k) * dz + float(i % 3) * dz/3. -dz + float(j % 2) * dz/3.   # lowest z coordinate starts below xy plane.
    return Point(x,y,z)

  @staticmethod
  def midpoint(p1,p2):
    return Point((p1.x + p2.x)/2., (p1.y + p2.y)/2., (p1.z + p2.z)/2.)

class Triangle:
  def __init__(self, p1, p2, p3):
    self.p1 = p1
    self.p2 = p2
    self.p3 = p3

  def __str__(self):
    return "%s\n%s\n%s\n" % (self.p1.__str__(), self.p2.__str__(), self.p3.__str__())