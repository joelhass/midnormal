## GradNormal 

This folder has files related to the MidNormal and GradNormal Meshing Algorithms
 
## PYTHON files 

gradNormal.py is a self contained python program that runs under Python 3.7.4
There are two additional required python files
region_definition.py
geometric_objects.py

6/24/20  Uploaded version that meshes surfaces of genus 0,1,2 and 5.
Also can remesh the bunny surface described via a object format file (.off)
The file "translated_bun.off" contains the Stanford bunny
with holes filled and resized to fit into the unit cube [0,1]^3

This python program is meant only as a proof of concept.
It can be used to reconstruct the examples in the paper
"Approximating isosurfaces by guaranteed-quality triangular meshes"
by J. Hass and M. Trnkova, that was presented at 
The Eurographics Symposium on Geometry Processing (SGP 2020).
There is much room to improve running time and efficiency.
It is not set up to input general surfaces or for general usage.
We expect this will come soon.

To run the program, first edit the file and choose a value for N.
This gives the number of tetrahedra used to tile the unit cube [0,1]^3
Choosing N results in 6N^3  tetrahdera.
On a MacBook Air, N=200 takes about 20 minutes.  
This involves a tiling of the cube by 52,931,340 tetrahedra

You can pick a shape parameter a also.
The default is a = math.sqrt(2.)/4. which is optimal for GradNormal.
Other choices are possible.  Only this parameter is discussed in the paper.

Then pick a surface to mesh by uncommenting one of the  options
#case = "sphere"
#case = "torus"
#case = "genus2"
#case = "genus5"
#case = "bunny"

Save the file and run the program by typing

python midN.py 

in a terminal window.  Be sure it is python version 3.


If one of the surfaces described by a formula is chosen,
the output of this program is three .off files

surfacePts.off   - output of the MidNormal algorithm

gsurfacePts.off - projection of output of the MidNormal algorithm to the level
set using a gradient estimate of the location

noquadSurfacePts.off - adjusts gsurfacePts.off as in GradNormal algorithm by
replacing 4 triangles neighboring valence four vertices with 2 triangles.

If case "bunny" is chosen, the relevant output is 
surfacePts.off   - output of the MidNormal algorithm
projected_bun.off - projection of output of the MidNormal algorithm to the 
original bunny mesh using a closest point projection, followed by 
replacing 4 triangles neighboring valence four vertices with 2 triangles.


The files are best viewed after removing duplicate vertices and 
orienting all faces coherently. This can be done with meshlab
or similar software.


 
## Mathematica files 

There are  four Mathematica files containing calculations used in
proofs of the MidNormal and GradNormal algorithms.

1. MidNormal.nb
Describes Goldberg tetrahedra as a family of tetrahedra that 
depends on a parameter “a”. 
Vertices of a tetrahedron depend on a.
Computes edges as vectors. Compute their length. 

Optimizes angles of normal discs and obtains a1, a2, a3, a4.

Computes edge length ratios

2. MidNormal_VectorDirections.nb
Defines six types of Goldberg tetrahedra unto parallel translation 
and for each tetrahedron 
computes directions of normal vectors for elementary normal discs.

3. Best_cross_sections.nb
Computes angles of elementary normal discs of a general tetrahedron for 
different choices of diagonals of quads. 

4. GradNormal.nb
Computes angles of MidNormal algorithm after projections onto the
faces of Goldberg’s tetrahedron 
in order to find the best value for parameter a. 
Also finds critical values for projected angles of GradNormal algorithm.


## 5. SlidNormal.nb (ADDED on 12/12/19)
Uses elementary normal discs of MidNormal algorithm and slides vertices of a 
mesh along edges of Goldberg’s tetrahedron. 


Then computes  max and  minimal angles among all possible triangles. 
There are 8 cases to consider that depend on a choice of a diagonal of a quad.


6.  (Added 1/25/2021)
The repository https://gitlab.com/bhass/new_normal has a much faster
and easy to use implementation that inputs an arbirary .off format file and
remeshes using the gradnormal algorithm. Details are given there.
